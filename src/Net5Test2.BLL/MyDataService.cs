﻿using System.Collections;
using System.Collections.Generic;
using Net5Test2.BLL.Data.Models;

namespace Net5Test2.BLL
{
    public interface IMyDataService
    {
        IEnumerable<User> GetUsers();
        void Save(User user);
    }

    public class MyDataService : IMyDataService
    {
        private readonly IDbClass _dbClass;

        public MyDataService(IDbClass dbClass)
        {
            _dbClass = dbClass;
        }

        public IEnumerable<User> GetUsers()
        {
            return _dbClass.GetAll();
        }

        public void Save(User user)
        {
            _dbClass.Save(user);
        }
    }
}