﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Net5Test2.BLL.Data.Models;

namespace Net5Test2.BLL
{
    public interface IDbClass
    {
        IEnumerable<User> GetAll();
        void Save(User user);
    }

    public class DbClass : IDbClass
    {
        private readonly MyDBContext _context;

        public DbClass(MyDBContext context)
        {
            _context = context;
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users.ToList();
        }

        public void Save(User user)
        {
            _context.Users.Add(user); 
            _context.SaveChanges();
        }
    }
}